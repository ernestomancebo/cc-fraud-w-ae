# cc-fraud-w-ae

Jupyter Notebook implementation of the project exposed in [Credit Card Fraud Detection using Deep Learning based on Auto-Encoder and Restricted Boltzmann Machine](https://pdfs.semanticscholar.org/01be/7624aa0e0251182593350a984411c2e5128a.pdf) academic paper.