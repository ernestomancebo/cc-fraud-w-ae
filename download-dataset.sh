#!/bin/bash
models="models/"
data="data/"
australian="australian"
german="german"
european="european"
european_filename="creditcard.Rdata.zip"

# Creating models structure
mkdir -p $models$australian $models$german $models$european

# Redefining folder structure - for code bellow ;) 
australian=$data$australian
german=$data$german
european=$data$european

# Creating data structure
mkdir -p $australian $german $european

# Downloads Australian Dataset into data/australian
cd $australian
curl \
    -O http://archive.ics.uci.edu/ml/machine-learning-databases/statlog/australian/australian.dat \
    -O http://archive.ics.uci.edu/ml/machine-learning-databases/statlog/australian/australian.doc 
cd -

# Downloads German Dataset into data/german
cd $german
curl \
    -O http://archive.ics.uci.edu/ml/machine-learning-databases/statlog/german/german.data \
    -O http://archive.ics.uci.edu/ml/machine-learning-databases/statlog/german/german.data-numeric \
    -O http://archive.ics.uci.edu/ml/machine-learning-databases/statlog/german/german.doc
cd -

# Downloads European Dataset into data/european

echo "Go to https://www.kaggle.com/mlg-ulb/creditcardfraud/version/2#creditcard.csv and ask to God the file manually."

# cd $european
# curl \
#     -O https://storage.googleapis.com/kaggle-data-sets/310/670/compressed/creditcard.Rdata.zip?GoogleAccessId=web-data@kaggle-161607.iam.gserviceaccount.com&Expires=1585989500&Signature=MpIkUeKYNm9UAVofQKByKt8sNWjSBAzTmnpj9ekZsCJlxMyQ8%2Fxs8qZToy0vu0zK5KGMi6iRNWgrVyeKfu6GHLNWOtYi6PfwmZg8N1JWIKGEEvZd7OCiSQVI7PMnxYWtwIIOQTweNzOFBfnB3ALVBF3g0XnftMxYv%2BddNIqcFtIBBsVeQ7EK57QaSGMODjtkyZCEilw5GJHnBv5kt69Dyeun6tjQ0MKCwqrEBD6KZ3X1LP5%2BEI3LPclXMB4okMUh%2F18mSKAQ%2BLW1q9W9BbdnwmxwQrl5u3sTmITe0wlHSNRJdfnaGzVXQygU7TknIOwtscCgaEmKN8tC44BnOsYtRg%3D%3D&response-content-disposition=attachment%3B+filename%3Dcreditcard.Rdata.zip
# unzip $european_filename
# rm -v !( $european_filename )
# cd -